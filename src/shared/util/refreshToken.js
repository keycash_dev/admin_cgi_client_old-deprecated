import { auth as axios } from '@/api'

const refresh = (refreshToken, email) => {
  return new Promise((resolve, reject) => {
    axios
      .post('/auth/refresh-token', {
        userName: email,
        refresh_token: refreshToken,
      })
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

export default refresh
