import api from '@/api'
import store from '@/store'

const setTheme = () => {
  const themes = []
  getTheme().then((response) => {
    themes.push(...response)
    setVuetify(themes)
  })
}

const setVuetify = (themes = []) => {
  let primary,
    secondary,
    tertiary,
    success,
    error,
    accent,
    gradient1,
    gradient2,
    header,
    disabled
  themes.map((el) => {
    if (el.attribute == 'primary') {
      primary = el.value
    } else if (el.attribute == 'secondary') {
      secondary = el.value
    } else if (el.attribute == 'tertiary') {
      tertiary = el.value
    } else if (el.attribute == 'success') {
      success = el.value
    } else if (el.attribute == 'error') {
      error = el.value
    } else if (el.attribute == 'accent') {
      accent = el.value
    } else if (el.attribute == 'gradient1') {
      gradient1 = el.value
    } else if (el.attribute == 'gradient2') {
      gradient2 = el.value
    } else if (el.attribute == 'header') {
      header = el.value
    } else if (el.attribute == 'disabled') {
      disabled = el.value
    }
  })
  store.commit('theme', {
    primary,
    secondary,
    tertiary,
    success,
    error,
    accent,
    gradient1,
    gradient2,
    header,
    disabled,
  })
}

const getTheme = () => {
  return new Promise((resolve, reject) => {
    getTag()
      .then((response) => {
        return api.get(`/themes/by-tag/${response}`)
      })
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

const getTag = () => {
  return new Promise((resolve, reject) => {
    const companies = []
    api
      .get(`/companies`)
      .then((response) => {
        companies.push(...response.data)
        const urlDomain = window.location.origin
        //undefined se não tiver
        const companie = companies.find((el) =>
          urlDomain.includes(el.name.toLowerCase())
        )
        resolve(companie !== undefined ? companie.name : 'Porto Seguro')
      })
      .catch((error) => {
        reject(error)
      })
  })
}

export default setTheme
