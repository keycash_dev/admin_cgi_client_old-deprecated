import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'
dayjs.extend(customParseFormat)

export const kcDate = {
  filters: {
    newDate(date) {
      return dayjs(date).format('DD/MM/YYYY')
    },
  },
  methods: {
    utcDate(date, outputFormat = 'YYYY-MM-DD HH:mm') {
      return date ? dayjs.utc(date).local().format(outputFormat) : null
    },
    systemDate(date, format = null, outputFormat = 'YYYY-MM-DD') {
      if (!date) return null

      const parsedDate = dayjs(date, 'DD/MM/YYYY')

      if (!format) return dayjs(parsedDate).format(outputFormat)

      return dayjs(parsedDate, format).format(outputFormat)
    },
    userDateTime(date, outputFormat = 'DD/MM/YYYY HH:mm') {
      return date ? dayjs(date).format(outputFormat) : null
    },

    userDate(date, outputFormat = 'DD/MM/YYYY') {
      if (date) {
        date = new Date(date)
        date = new Date(
          date.getTime() + Math.abs(date.getTimezoneOffset() * 60000)
        )
        return dayjs(date).format(outputFormat)
      }
      return null
    },
  },
}

export const changeTheme = {
  methods: {
    changeTheme({
      primary,
      secondary,
      tertiary,
      success,
      error,
      accent,
      gradient1,
      gradient2,
      header,
      disabled,
    }) {
      this.$vuetify.theme.themes.light.primary = primary
      this.$vuetify.theme.themes.light.secondary = secondary
      this.$vuetify.theme.themes.light.tertiary = tertiary
      this.$vuetify.theme.themes.light.success = success
      this.$vuetify.theme.themes.light.error = error
      this.$vuetify.theme.themes.light.accent = accent
      this.$vuetify.theme.themes.light.gradient1 = gradient1
      this.$vuetify.theme.themes.light.gradient2 = gradient2
      this.$vuetify.theme.themes.light.header = header
      this.$vuetify.theme.themes.light.disabled = disabled
    },
  },
}

export const moduleAccess = {
  data() {
    return {
      permissions: {
        canRead: false,
        canWrite: false,
      },
    }
  },
  methods: {
    setUserPersmission(module) {
      const user = JSON.parse(localStorage.getItem('user'))
      if (user === null) {
        this.permissions = { canRead: false, canWrite: false }
      } else {
        const canRead =
          user.modules.findIndex((el) => el == `[read]${module}`) !== -1
            ? true
            : false
        const canWrite =
          user.modules.findIndex((el) => el == `[write]${module}`) !== -1
            ? true
            : false
        this.permissions = { canRead, canWrite }
      }
    },
    getUserModulesValuesByTag(tag, isNumber = false) {
      const user = JSON.parse(localStorage.getItem('user'))
      if (user === null) return []
      const documentsTypes = user.modules.filter((el) => el.includes(tag))
      documentsTypes.map((el, index) => {
        documentsTypes[index] = isNumber
          ? +el.replace(tag, '')
          : el.replace(tag, '')
      })
      return [...new Set(documentsTypes)]
    },
  },
}
