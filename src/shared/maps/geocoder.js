const geocode = (fullAddress) => {
  return new Promise((resolve, reject) => {
    const geocoder = new google.maps.Geocoder()
    geocoder.geocode({ address: fullAddress }, ([response], status) => {
      if (status === 'OK') {
        return resolve(response)
      } else {
        let error = 'UNKNOWN_ERROR'

        switch (status) {
          case 'ZERO_RESULTS':
            error = 'ZERO_RESULTS: No results found'
            break
          case 'OVER_DAILY_LIMIT':
            error =
              'OVER_DAILY_LIMIT: Check if api key is missing, invalid, billing has not been enabled, cap has been exceedded or creditcard is expired'
            break
          case 'OVER_QUERY_LIMIT':
            error = 'OVER_QUERY_LIMIT: You reached your quota of requests'
            break
          case 'REQUEST_DENIED':
            error = 'REQUEST_DENIED: Request was denied by the server'
            break
          case 'INVALID_REQUEST':
            error =
              'INVALID_REQUEST: Check if address, components or latlng is missing'
            break
          default:
            break
        }

        return reject(error)
      }
    })
  })
}

export default geocode
