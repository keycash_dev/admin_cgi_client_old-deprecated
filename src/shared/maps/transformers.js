const autocompleteTransform = (addressData, placeResultData) => {
  let place = null

  if (placeResultData) {
    place = {
      fullAddress: placeResultData.formatted_address,
      placeId: placeResultData.place_id,
      name: placeResultData.name,
      streetNumber: _getPlaceAttribute(placeResultData, 'street_number'),
      streetName: _getPlaceAttribute(placeResultData, 'route', true),
      district: _getPlaceAttribute(placeResultData, 'sublocality_level_1'),
      city: _getPlaceAttribute(placeResultData, 'administrative_area_level_2'),
      state: _getPlaceAttribute(placeResultData, 'administrative_area_level_1'),
      country: _getPlaceAttribute(placeResultData, 'country', true),
      zipcode: _getPlaceAttribute(placeResultData, 'postal_code'),
      lat: addressData.latitude,
      lng: addressData.longitude,
    }
  }

  return place
}

const geacoderTranform = (placeResultData) => {
  let place = null

  if (placeResultData) {
    place = {
      fullAddress: placeResultData.formatted_address,
      placeId: placeResultData.place_id,
      name: placeResultData.name,
      streetNumber: _getPlaceAttribute(placeResultData, 'street_number'),
      streetName: _getPlaceAttribute(placeResultData, 'route', true),
      district: _getPlaceAttribute(placeResultData, 'sublocality_level_1'),
      city: _getPlaceAttribute(placeResultData, 'administrative_area_level_2'),
      state: _getPlaceAttribute(placeResultData, 'administrative_area_level_1'),
      country: _getPlaceAttribute(placeResultData, 'country', true),
      zipcode: _getPlaceAttribute(placeResultData, 'postal_code'),
      lat: _getGeo(placeResultData, 'lat'),
      lng: _getGeo(placeResultData, 'lng'),
      plusCode: placeResultData.plus_code,
    }
  }

  return place
}

const _getGeo = (results, type) => {
  if (results.geometry && results.geometry.location) {
    return results.geometry.location[type]()
  }
  return null
}
const _getPlaceAttribute = (results, type, long = false) => {
  for (let r of results.address_components) {
    if (r.types.indexOf(type) !== -1) {
      return long ? r.long_name : r.short_name
    }
  }
  return ''
}

export default {
  autocompleteTransform,
  geacoderTranform,
}
