import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import './styles/kc-styles.scss'
import VCurrencyField from 'v-currency-field'
import { VTextField } from 'vuetify/lib'
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete' //Globally import VTextField
import VueTheMask from 'vue-the-mask'

Vue.use(VueTheMask)

Vue.component('v-text-field', VTextField)

Vue.use(require('vue-dayjs'))

Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: process.env.VUE_APP_MAPS_KEY, // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
  version: '3.41', // Optional
})

Vue.use(VCurrencyField, {
  locale: 'pt-BR',
  decimalLength: 2,
  autoDecimalMode: true,
  min: null,
  max: null,
  defaultValue: 0,
  valueAsInteger: false,
  allowNegative: true,
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app')
