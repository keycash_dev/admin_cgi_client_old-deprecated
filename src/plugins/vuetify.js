import Vue from 'vue'
import Vuetify, { VTextField } from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'
import '@fortawesome/fontawesome-free/css/all.css'
import pt from 'vuetify/lib/locale/pt'
import en from 'vuetify/lib/locale/en'
import { KcIcons } from 'keycash-icons'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'fa',
    values: {
      ...KcIcons,
    },
  },
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#221F47',
        secondary: '#5E95B7',
        accent: '#FF0094',
        success: colors.green.lighten1,
        error: colors.red.darken1,
        /* primary: '#00AEEF',
        secondary: '#4c4c4c',
        tertiary: '#F5F5F5',
        success: '#46A166',
        error: '#E34525',
        accent: '#F09D1F',
        gradient1: '#2FC5FF',
        gradient2: '#00AEEF',
        header: '#004E70',
        disabled: '#86c2d9', */
      },
    },
  },
  lang: {
    locales: { en, pt },
    current: 'pt',
  },
  components: {
    // Must be added explicitly beacause of VuetifyGoogleAutocomplete
    VTextField,
  },
})
