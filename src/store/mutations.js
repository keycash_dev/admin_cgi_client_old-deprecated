import jwtDecode from 'jwt-decode'

const mutations = {
  login(state, value) {
    localStorage.token = value.accessToken
    localStorage.refreshToken = value.refreshToken
    state.isAuthenticated = true
  },
  user(state, value) {
    const user = jwtDecode(value.accessToken)
    localStorage.user = JSON.stringify(user)
    state.user = user
  },
  logout(state) {
    state.isAuthenticated = false
    localStorage.clear()
  },
  showSnackbar(state, value) {
    state.snackbar.show = true
    state.snackbar.text = value.text
    state.snackbar.timeout = value.timeout ? value.timeout : 3000
    state.snackbar.color = value.color ? value.color : 'primary'
  },
  theme(state, value) {
    state.theme = value
  },
  checkAuthentication(state) {
    state.isAuthenticated = localStorage.getItem('token') ? true : false
  },
}

export default mutations
