const state = {
  isAuthenticated: false,
  snackbar: {
    show: false,
    text: '',
    timeout: 3000,
    color: 'primary',
  },
  theme: {
    primary: '',
    secondary: '',
    tertiary: '',
    success: '',
    error: '',
    accent: '',
    gradient1: '',
    gradient2: '',
    header: '',
    disabled: '',
  },
}

export default state
