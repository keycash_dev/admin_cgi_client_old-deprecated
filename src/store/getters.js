const getters = {
  isAuthenticated: (state) => {
    return state.isAuthenticated
  },
  user: (state) => {
    return localStorage.getItem('user')
      ? JSON.parse(localStorage.getItem('user'))
      : state.user
  },
  snackbar: (state) => state.snackbar,
}

export default getters
