import axios from 'axios'
import refresh from '@/shared/util/refreshToken'

const API_URL = process.env.VUE_APP_API_KEYCASH

const api = axios.create({
  headers: {
    'Content-Type': 'application/json',
  },
  baseURL: API_URL,
})

api.interceptors.request.use(
  (config) => {
    const token = localStorage.token
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    return config
  },
  (err) => {
    return Promise.reject(err)
  }
)

api.interceptors.response.use(
  (response) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response
  },
  (error) => {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (error.response && error.response.status === 401) {
      const { url, method, data } = error.response.config
      return refresh(
        localStorage.refreshToken,
        JSON.parse(localStorage.user).email
      )
        .then((token) => {
          localStorage.setItem('token', token.accessToken)
          return api({
            headers: {
              Authorization: `Bearer ${token.accessToken}`,
              'content-type': 'application/json',
            },
            method,
            url,
            data,
          })
        })
        .then((response) => {
          return Promise.resolve(response)
        })
        .catch((error) => {
          Promise.reject(error)
        })
    } else {
      return Promise.reject(error)
    }
  }
)

export default api
