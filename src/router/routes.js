const header = () => import('@/components/Header.vue')

const routes = [
  {
    path: '/login',
    name: 'login',
    components: {
      default: () => import('@/views/Login.vue'),
    },
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/reset',
    name: 'reset',
    components: {
      default: () => import('@/views/Reset.vue'),
    },
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/home',
    name: 'home',
    components: {
      header,
      default: () => import('@/views/Home.vue'),
    },
    meta: {
      title: 'Home',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
      ],
    },
  },
  {
    path: '/forbidden',
    name: 'forbidden',
    components: {
      default: () => import('@/views/Forbidden.vue'),
    },
    meta: {
      requiresAuth: false,
    },
  },
  {
    path: '/profiles',
    name: 'profiles',
    components: {
      header,
      default: () => import('@/views/Profiles.vue'),
    },
    meta: {
      title: 'Perfis',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'profiles',
          alias: 'Perfis',
        },
      ],
    },
  },
  {
    path: '/resources',
    name: 'resources',
    components: {
      header,
      default: () => import('@/views/Resources.vue'),
    },
    meta: {
      title: 'Recursos',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'resources',
          alias: 'Recursos',
        },
      ],
    },
  },
  {
    path: '/cities',
    name: 'cities',
    components: {
      header,
      default: () => import('@/views/Cities.vue'),
    },
    meta: {
      title: 'Cidades',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'cities',
          alias: 'Cidades',
        },
      ],
    },
  },
  {
    path: '/permissions',
    name: 'permissions',
    components: {
      header,
      default: () => import('@/views/Permissions.vue'),
    },
    meta: {
      title: 'Permissões',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'permissions',
          alias: 'Permissões',
        },
      ],
    },
  },
  {
    path: '/modules',
    name: 'modules',
    components: {
      header,
      default: () => import('@/views/Modules.vue'),
    },
    meta: {
      title: 'Módulos',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'modules',
          alias: 'Módulos',
        },
      ],
    },
  },
  {
    path: '/profiles/cadastre/:id?',
    name: 'profiles-cadastre',
    components: {
      header,
      default: () => import('@/views/ProfilesCadastre.vue'),
    },
    meta: {
      title: 'Perfis',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'profiles',
          alias: 'Perfis',
        },
        {
          name: 'profiles-cadastre',
          alias: 'Cadastro de Perfil',
        },
      ],
    },
  },
  {
    path: '/users-update/:id',
    name: 'users-update',
    components: {
      header,
      default: () => import('@/views/UsersUpdate.vue'),
    },
    meta: {
      title: 'Usuários',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'users',
          alias: 'Usuários',
        },
        {
          name: 'users-update',
          alias: 'Atualizar Usuário',
        },
      ],
    },
  },
  {
    path: '/users',
    name: 'users',
    components: {
      header,
      default: () => import('@/views/Users.vue'),
    },
    meta: {
      title: 'Usuários',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'users',
          alias: 'Usuários',
        },
      ],
    },
  },
  {
    path: '/status-tracking',
    name: 'status-tracking',
    components: {
      header,
      default: () => import('@/views/StatusTracking.vue'),
    },
    meta: {
      title: 'Análise',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'status-tracking',
          alias: 'Análise',
        },
      ],
    },
  },
  {
    path: '/opportunities',
    name: 'opportunities',
    components: {
      header,
      default: () => import('@/views/Opportunities.vue'),
    },
    meta: {
      title: 'Oportunidades',
      requiresAuth: false,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'opportunities',
          alias: 'Oportunidades',
        },
      ],
    },
  },
  {
    path: '/dashboards',
    name: 'dashboards',
    components: {
      header,
      default: () => import('@/views/Dashboards.vue'),
    },
    meta: {
      title: 'Dashboards',
      requiresAuth: false,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'dashboards',
          alias: 'Dashboards',
        },
      ],
    },
  },
  {
    path: '/companies',
    name: 'companies',
    components: {
      header,
      default: () => import('@/views/Companies.vue'),
    },
    meta: {
      title: 'Empresas',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'companies',
          alias: 'Empresas',
        },
      ],
    },
  },
  {
    path: '/register-opportunity',
    name: 'register-opportunity',
    components: {
      header,
      default: () => import('@/views/RegisterOpportunity.vue'),
    },
    meta: {
      title: 'Cadastro de Oportunidades',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'register-opportunity',
          alias: 'Cadastro de Oportunidades',
        },
      ],
    },
  },
  {
    path: '/social-media',
    name: 'social-media',
    components: {
      header,
      default: () => import('@/views/SocialMedia.vue'),
    },
    meta: {
      title: 'Cadastro de Mídias Sociais',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'social-media',
          alias: 'Cadastro de Mídias Sociais',
        },
      ],
    },
  },
  {
    path: '/faq',
    name: 'faq',
    components: {
      header,
      default: () => import('@/views/Faq.vue'),
    },
    meta: {
      title: 'Cadastro de FAQ',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'faq',
          alias: 'Cadastro de FAQ',
        },
      ],
    },
  },
  {
    path: '/themes/:id',
    name: 'themes',
    components: {
      header,
      default: () => import('@/views/Themes.vue'),
    },
    meta: {
      title: 'Temas',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'companies',
          alias: 'Empresas',
        },
        {
          name: 'themes',
          alias: 'Temas',
        },
      ],
    },
  },
  {
    path: '/setup',
    name: 'setup',
    components: {
      header,
      default: () => import('@/views/Setup.vue'),
    },
    meta: {
      title: 'Parâmetros do Sistema',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
        {
          name: 'setup',
          alias: 'Parâmetros do Sistema',
        },
      ],
    },
  },
  {
    path: '/admin',
    name: 'admin',
    components: {
      header,
      default: () => import('@/views/Admin.vue'),
    },
    meta: {
      title: 'Admin',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'admin',
          alias: 'Administrador',
        },
      ],
    },
  },
  {
    path: '/review-cci/:opportunityId',
    name: 'review-cci',
    components: {
      header,
      default: () => import('@/views/ReviewCCI.vue'),
    },
    meta: {
      title: 'Review CCI',
      requiresAuth: false,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'review-cci',
          alias: 'Revisar CCI',
        },
      ],
    },
  },
  {
    path: '*',
    redirect: '/home',
  },
  {
    path: '/taxes',
    name: 'taxes',
    components: {
      header,
      default: () => import('@/views/Taxes.vue'),
    },
    meta: {
      title: 'Taxas',
      requiresAuth: true,
      breadcrumb: [
        {
          name: 'home',
          alias: 'Home',
        },
        {
          name: 'taxes',
          alias: 'Taxas',
        },
      ],
    },
  },
]
export default routes
